﻿using UnityEngine;
using System.Collections;


public class GameStartLogic : MonoBehaviour {

	public Transform Camera;
	public Transform UnityChan;
	public Animator newAnimator;
	public Animation oldAnimation;


	// Use this for initialization
	void Start () {
		newAnimator.enabled = false;
		oldAnimation.enabled = true;
		Invoke ("DelayPlay",1);
	
	}

	public void DelayPlay()
	{
		AnimatorTimeline.Play ("Take1");
		Invoke ("AnimationDone",AnimatorTimeline.totalTime);
	}

	public void AnimationDone()
	{
		Debug.Log ("Animation Done");
		newAnimator.enabled = true;
		oldAnimation.enabled = false;
		Camera.transform.parent = UnityChan;
	}
}
